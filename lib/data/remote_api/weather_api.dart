import 'package:test_application/core/network/api_service.dart';
import 'package:test_application/data/model/weather_dto.dart';

abstract class WeatherApi {
  Future<WeatherDto> fetchWeather(num lat, num lon);
}

class WeatherApiImpl implements WeatherApi {
  final MyApiService apiService;

  WeatherApiImpl({required this.apiService});

  @override
  Future<WeatherDto> fetchWeather(num lat, num lon) async {
    try {
      final res = await apiService.get('weather', queryParameters: {
        'lat': lat,
        'lon': lon,
        'appId': 'dd6b83206991a24b9af81bf946629042'
      });

      return WeatherDto.fromJson(res);

    } catch (e) {
      rethrow;
    }
  }
}
