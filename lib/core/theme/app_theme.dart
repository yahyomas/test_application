import 'package:flutter/material.dart';

class AppTheme {
  static final ThemeData lightTheme = ThemeData.light().copyWith(
    colorScheme: ColorScheme.fromSeed(
      seedColor: Colors.blue,
      primary: Colors.blue,
    ),
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    useMaterial3: true,
    iconTheme: const IconThemeData(color: Colors.white),
    appBarTheme: const AppBarTheme(
      centerTitle: true,
      backgroundColor: Colors.blueAccent,
      titleTextStyle: TextStyle(
          fontWeight: FontWeight.w500, fontSize: 24, color: Colors.white),
    ),
  );

  static final ThemeData darkTheme = ThemeData.dark().copyWith(
    colorScheme: ColorScheme.fromSeed(
      seedColor: Colors.blue,
      primary: Colors.tealAccent,
    ),
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
    useMaterial3: true,
    appBarTheme: const AppBarTheme(
      centerTitle: true,
      backgroundColor: Colors.black12,
      elevation: 2,
      titleTextStyle: TextStyle(
          fontWeight: FontWeight.w500, fontSize: 24, color: Colors.white),
    ),
  );
}
