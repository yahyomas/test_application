import 'package:dio/dio.dart';

class DioClient {
  static Dio myDio = _createDio();
  static Dio? _myDio;

  static final myFamilyDioOptions = BaseOptions(
      baseUrl: 'https://api.openweathermap.org/data/2.5/',
      connectTimeout: const Duration(milliseconds: 10000),
      receiveTimeout: const Duration(milliseconds: 10000),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
      responseType: ResponseType.json);

  static final logInterceptor = LogInterceptor(
    request: true,
    responseBody: true,
    requestBody: true,
    requestHeader: true,
  );

  static Dio _createDio() {
    _myDio ??= Dio(myFamilyDioOptions)..interceptors.add(logInterceptor);
    return _myDio!;
  }
}
