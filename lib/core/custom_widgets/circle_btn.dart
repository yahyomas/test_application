import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  final VoidCallback onTap;
  final double size;
  final IconData iconData;

  const CircleButton(
      {Key? key,
      required this.onTap,
      required this.size,
      required this.iconData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 54,
      width: 54,
      child: Center(
        child: InkWell(
          onTap: onTap,
          child: Container(
            decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primary,
                shape: BoxShape.circle),
            child: AnimatedSize(
              curve: Curves.decelerate,
              duration: const Duration(milliseconds: 200),
              child: SizedBox(
                height: size,
                width: size,
                child: Icon(
                  iconData,
                  size: size / 3,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
