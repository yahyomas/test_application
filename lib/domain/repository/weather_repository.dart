import 'package:test_application/data/model/weather_dto.dart';
import 'package:test_application/data/remote_api/weather_api.dart';

abstract class WeatherRepository {
  Future<WeatherDto> fetchWeather(num lat, num lon);
}

class WeatherRepositoryImpl implements WeatherRepository {
  final WeatherApiImpl api;

  WeatherRepositoryImpl({required this.api});

  @override
  Future<WeatherDto> fetchWeather(num lat, num lon) async {
    try {
      return await api.fetchWeather(lat, lon);
    } catch (e) {
      rethrow;
    }
  }
}
