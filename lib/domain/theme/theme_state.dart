import 'package:flutter/material.dart';

sealed class ThemeState {}

class SuccessState extends ThemeState {
  final ThemeData theme;

  SuccessState({required this.theme});
}
