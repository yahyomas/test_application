import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_application/core/theme/app_theme.dart';

import 'theme_event.dart';
import 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc() : super(SuccessState(theme: AppTheme.lightTheme)) {
    on<OnThemeChanged>(_onThemeChanged);
  }

  FutureOr<void> _onThemeChanged(
      OnThemeChanged event, Emitter<ThemeState> emit) {
    final theme = (state as SuccessState).theme;

    if (theme == AppTheme.lightTheme) {
      emit(SuccessState(theme: AppTheme.darkTheme));
    } else {
      emit(SuccessState(theme: AppTheme.lightTheme));
    }
  }
}
