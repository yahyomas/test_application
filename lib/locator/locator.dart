import 'package:get_it/get_it.dart';
import 'package:test_application/core/network/api_service.dart';
import 'package:test_application/data/remote_api/weather_api.dart';
import 'package:test_application/domain/repository/weather_repository.dart';

final getIt = GetIt.instance;

void setUpLocator({
  String? environment,
}) {
// Register dependencies

  //data sources
  getIt.registerLazySingleton(() => MyApiService());
  getIt.registerLazySingleton(() => WeatherApiImpl(apiService: getIt()));

  //repositories
  getIt.registerLazySingleton(() => WeatherRepositoryImpl(api: getIt()));
}
