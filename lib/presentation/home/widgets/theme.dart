import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_application/core/custom_widgets/circle_btn.dart';
import 'package:test_application/domain/theme/theme_bloc.dart';
import 'package:test_application/domain/theme/theme_event.dart';

class ThemeBtn extends StatelessWidget {
  const ThemeBtn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeBloc bloc = context.watch<ThemeBloc>();
    return Positioned(
      bottom: 24,
        left: 24,
        child: CircleButton(
      onTap: () => bloc.add(OnThemeChanged()),
      iconData: Icons.palette_rounded,
      size: 54,
    ));
  }
}
