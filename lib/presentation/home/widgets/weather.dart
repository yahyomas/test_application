import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_application/core/custom_widgets/circle_btn.dart';
import 'package:test_application/presentation/home/bloc/weather/weather_bloc.dart';
import 'package:test_application/presentation/home/bloc/weather/weather_event.dart';

class WeatherBtn extends StatelessWidget {
  const WeatherBtn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final WeatherBloc bloc = context.watch();
    return Positioned(
      bottom: 120,
      left: 24,
      child: CircleButton(
        onTap: () => bloc.add(OnFetchWeather()),
        iconData: Icons.cloud,
        size: 54,
      ),
    );
  }
}
