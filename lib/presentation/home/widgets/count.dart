import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_application/presentation/home/bloc/counter/counter_bloc.dart';
import 'package:test_application/presentation/home/bloc/counter/counter_bloc_state.dart';

class Count extends StatelessWidget {
  const Count({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CounterBloc, CounterState>(builder: (context, state) {
      if (state is SuccessState) {
        return Column(
          children: [
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              state.counter.toString(),
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        );
      }
      return const SizedBox();
    });
  }
}
