import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_application/core/custom_widgets/circle_btn.dart';
import 'package:test_application/presentation/home/bloc/counter/counter_bloc.dart';
import 'package:test_application/presentation/home/bloc/counter/counter_bloc_event.dart';
import 'package:test_application/presentation/home/bloc/counter/counter_bloc_state.dart';

class DecrementBtn extends StatelessWidget {
  const DecrementBtn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final CounterBloc bloc = context.watch<CounterBloc>();

    return BlocBuilder<CounterBloc, CounterState>(builder: (context, state) {
      if (state is SuccessState) {
        return Positioned(
          bottom: 24,
          right: 24,
          child: CircleButton(
            onTap: () => bloc.add(OnDecrementPressed()),
            iconData: Icons.remove,
            size: state.decrementBtnSize,
          ),
        );
      }
      return const SizedBox();
    });
  }
}
