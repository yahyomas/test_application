import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_application/domain/theme/theme_bloc.dart';
import 'package:test_application/locator/locator.dart';
import 'package:test_application/presentation/home/bloc/counter/counter_bloc.dart';
import 'package:test_application/presentation/home/bloc/weather/weather_bloc.dart';
import 'package:test_application/presentation/home/widgets/count.dart';
import 'package:test_application/presentation/home/widgets/decrement.dart';
import 'package:test_application/presentation/home/widgets/increment.dart';
import 'package:test_application/presentation/home/widgets/theme.dart';
import 'package:test_application/presentation/home/widgets/weather_info.dart';

import 'widgets/weather.dart';

class HomeScreen extends StatelessWidget {
  static const String route = '/';

  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeBloc themeBloc = context.watch<ThemeBloc>();
    return MultiBlocProvider(
      providers: [
        BlocProvider<CounterBloc>(
            create: (context) => CounterBloc(themeBloc: themeBloc)),
        BlocProvider<WeatherBloc>(
            create: (context) => WeatherBloc(repository: getIt())),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Weather Counter'),
        ),
        body: const Stack(
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  WeatherInfo(),
                  SizedBox(
                    height: 24,
                  ),
                  Count(),
                ],
              ),
            ),
            IncrementBtn(),
            DecrementBtn(),
            ThemeBtn(),
            WeatherBtn(),
          ],
        ),
      ),
    );
  }
}
