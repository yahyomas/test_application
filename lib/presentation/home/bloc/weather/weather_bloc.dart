import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:test_application/core/location/location_service.dart';
import 'package:test_application/domain/repository/weather_repository.dart';
import 'package:test_application/presentation/home/bloc/weather/weather_event.dart';
import 'package:test_application/presentation/home/bloc/weather/weather_state.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepositoryImpl repository;

  WeatherBloc({required this.repository})
      : super(SuccessState(text: 'Press the icon to get your location')) {
    on<OnFetchWeather>(_fetchWeather);
  }

  FutureOr<void> _fetchWeather(
      OnFetchWeather event, Emitter<WeatherState> emit) async{
    emit(LoadingState());

   await  _fetchData(emit);
  }

  Future<void> _fetchData(emit) async {
     try{
       final Position position = await LocationService.determinePosition();

       final res = await repository.fetchWeather(position.latitude, position.longitude);

       emit(SuccessState(text: 'Temperature is: ${res.main.temp}F'));
     }catch(e){
       emit(ErrorState(message: 'Something went wrong, try again'));
     }
  }


}
