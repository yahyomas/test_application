sealed class WeatherState {}

class SuccessState extends WeatherState {
  final String text;

  SuccessState({required this.text});
}

class LoadingState extends WeatherState {}

class ErrorState extends WeatherState {
  final String message;
  ErrorState({required this.message});
}
