import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_application/core/theme/app_theme.dart';
import 'package:test_application/domain/theme/theme_bloc.dart';
import 'package:test_application/domain/theme/theme_state.dart' as theme_state;
import 'package:test_application/presentation/home/bloc/counter/counter_bloc_event.dart';
import 'package:test_application/presentation/home/bloc/counter/counter_bloc_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  final ThemeBloc themeBloc;

  bool _isDarkMode = false;

  CounterBloc({required this.themeBloc})
      : super(SuccessState(
            counter: 0, incrementBtnSize: 54, decrementBtnSize: 0)) {
    on<OnDecrementPressed>(_onDecrementPressed);
    on<OnIncrementPressed>(_onIncrementPressed);

    themeBloc.stream.listen((event) {
      _isDarkMode = (event is theme_state.SuccessState) &&
          event.theme == AppTheme.darkTheme;
    });
  }

  FutureOr<void> _onIncrementPressed(
      OnIncrementPressed event, Emitter<CounterState> emit) {
    int count;
    if(_isDarkMode){
       count = (state as SuccessState).counter + 2;
    }
    else{
       count = (state as SuccessState).counter + 1;
    }
    double decrementBtnSize = (state as SuccessState).decrementBtnSize;
    double incrementBtnSize = (state as SuccessState).incrementBtnSize;
    if (count > 9) {
      incrementBtnSize = 0;
      count = 10;
    }
    if (count > 0) {
      decrementBtnSize = 54;
    }
    emit(SuccessState(
        counter: count,
        decrementBtnSize: decrementBtnSize,
        incrementBtnSize: incrementBtnSize));
  }

  FutureOr<void> _onDecrementPressed(
      OnDecrementPressed event, Emitter<CounterState> emit) {
    int count;
    if(_isDarkMode){
      count = (state as SuccessState).counter - 2;
    }
    else{
      count = (state as SuccessState).counter - 1;
    }
    double decrementBtnSize = (state as SuccessState).decrementBtnSize;
    double incrementBtnSize = (state as SuccessState).incrementBtnSize;
    if (count < 1) {
      decrementBtnSize = 0;
      count = 0;
    }
    if (count < 10) {
      incrementBtnSize = 54;
    }
    emit(SuccessState(
        counter: count,
        decrementBtnSize: decrementBtnSize,
        incrementBtnSize: incrementBtnSize));
  }
}
