sealed class CounterEvent {}

class OnIncrementPressed extends CounterEvent{}


class OnDecrementPressed extends CounterEvent{}