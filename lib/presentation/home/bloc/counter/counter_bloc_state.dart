sealed class CounterState {}

class SuccessState extends CounterState {
  final int counter;
  final double incrementBtnSize;
  final double decrementBtnSize;

  SuccessState(
      {required this.counter,
      required this.decrementBtnSize,
      required this.incrementBtnSize});
}
