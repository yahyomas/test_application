import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_application/domain/theme/theme_bloc.dart';
import 'package:test_application/locator/locator.dart';
import 'package:test_application/presentation/home/home_screen.dart';

import 'domain/theme/theme_state.dart';

void main() {
  setUpLocator();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<ThemeBloc>(
          create: (context) => ThemeBloc(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeBloc, ThemeState>(
      builder: (context, ThemeState state) {
        if (state is SuccessState) {
          return MaterialApp(
            title: 'Flutter Demo',
            theme: state.theme,
            routes: {
              HomeScreen.route: (context) => const HomeScreen(),
            },
          );
        }
        return const MaterialApp();
      },
    );
  }
}
